<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Entity\Image;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;

class EntityController extends Controller
{
    /**
     * @Route("/tape/img/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newImageAction(Request $request)
    {
        $image = new Image();
        $form = $this->createForm('AppBundle\Form\ImageType', $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->getData();
            $image->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();
        }
        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/{id}/follow")
     * @var $id int
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFollower($id)
    {

        $follower = $this->getUser();
        if ($follower) {
                $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
                $em = $this->getDoctrine()->getManager();
                $user->addFollower($follower);
                $em->persist($user);
                $em->flush();
        }
        return $this->redirectToRoute('app_pagescontroller_profile', [
            'id' => $id,
        ]);
    }

    /**
     * @Route("/tape/follow/{id}/rm")
     * @var $id int
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFollower($id)
    {
        $follower = $this->getUser();
        if ($follower) {
                $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
                $em = $this->getDoctrine()->getManager();
                $user->removeFollower($follower);
                $em->persist($user);
                $em->flush();
        }

        return $this->redirectToRoute('app_pagescontroller_profile', [
            'id' => $id,
        ]);
    }

    /**
     * @Route("/tape/{id}/comment-add")
     *
     * @var int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addCommentAction($id, Request $request)
    {
        /**
         * @var $comment Comment
         */
        $image = $this->getDoctrine()->getRepository('AppBundle:Image')->find($id);
        if ($image) {
            $comment = new Comment();
            $form = $this->createForm('AppBundle\Form\CommentType', $comment);
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $comment = $form->getData();
                $comment->setUser($this->getUser());
                $comment->setImage($image);
                $comment->setTime(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();
            }
        }


        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/img/{id}/like")
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addlike($id)
    {
        $image = $this->getDoctrine()->getRepository('AppBundle:Image')->find($id);
        if ($image) {
            $em = $this->getDoctrine()->getManager();
            /**
             * @var $likers ArrayCollection
             */
            $likers = $image->getLikers();
            if ($likers->contains($this->getUser())) {
                $image->removeLiker($this->getUser());
                $em->persist($image);
            } else {
                $image->addLiker($this->getUser());
                $em->persist($image);
            }
            $em->flush();
        }

        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/{id}/avatar-change")
     * @Method("POST")
     * @param $id int
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeAvatar($id, Request $request)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user) {
            if ($user->getId() == $this->getUser()->getId()) {
                $form = $this->createFormBuilder($user)->add('imageFile', FileType::class)->getForm();
                $form->handleRequest($request);
                $user = $form->getData();
                $user->setUpdateAt(new \DateTime('now', new \DateTimeZone('Asia/Bishkek')));
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->redirectToRoute('app_pagescontroller_profile', [
            'id' => $id]);
    }
}