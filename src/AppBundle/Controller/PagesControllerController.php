<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class PagesControllerController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_USER', $this->getUser())) {
            return $this->redirectToRoute('app_pagescontroller_tape');
        }
        return $this->redirectToRoute('fos_user_registration_register');
    }

    /**
     * @Route("/tape")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tapeAction()
    {
        $image = new Image();
        $form = $this->createForm('AppBundle\Form\ImageType', $image);
        $comment = new Comment();
        $form_comment = $this->createForm('AppBundle\Form\CommentType', $comment);

        $usersId = $this->getUser()->getUserFollowId();

        $images = $this->getDoctrine()->getRepository('AppBundle:Image')->findBy(['user' => $usersId], ['id' => 'DESC']);

        return $this->render('@App/PagesController/index.html.twig', [
            'images' => $images,
            'form' => $form->createView(),
            'formComment' => $form_comment,
        ]);
    }

    /**
     * @Route("/tape/{id}/profile")
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction($id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user) {
            $form = $this->createFormBuilder($user)->add('imageFile', FileType::class)->add('Сохранить', SubmitType::class)->getForm();

            return $this->render('@App/PagesController/profile.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
            ]);
        }
        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/{id}/followers")
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followerListAction($id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user) {
            $users = $user->getFollowers();
            return $this->render('@App/PagesController/list.html.twig', [
                'users' => $users,
            ]);
        }
        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/{id}/user-follow")
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userFollowListAction($id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user) {
            $users = $user->getUserFollow();
            return $this->render('@App/PagesController/list.html.twig', [
                'users' => $users,
            ]);
        }
        return $this->redirectToRoute('app_pagescontroller_tape');
    }

    /**
     * @Route("/tape/search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(['username' => $request->get('searchName')]);

        return $this->render('@App/PagesController/list.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/tape/img/{id}/likers")
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likersListAction($id)
    {
        $image = $this->getDoctrine()->getRepository('AppBundle:Image')->find($id);
        if ($image) {
            $likers = $image->getLikers();
            return $this->render('@App/PagesController/list.html.twig', [
                'users' => $likers
            ]);
        }
        return $this->redirectToRoute('app_pagescontroller_tape');

    }
}
