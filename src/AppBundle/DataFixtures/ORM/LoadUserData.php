<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('12345')
            ->setEmail('2547437@gmail.com')
            ->setPassword(password_hash('12345', PASSWORD_DEFAULT))
            ->setEnabled(true);

        $manager->persist($user);

        $user = new User();
        $user->setUsername('zxcasd')
            ->setEmail('12345@mail.ru')
            ->setPassword(password_hash('12345', PASSWORD_DEFAULT))
            ->setEnabled(true);


        $manager->persist($user);

        $manager->flush();
    }
}
