<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Images
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImagesRepository")
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User",inversedBy="id")
     */
    private $likers;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="images_file", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     * @ORM\Column(name="image",type="string")
     */
    private $image;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->likers = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
     */
    private $user;

    /**
     * @var Comment[]
     * @ORM\OneToMany(targetEntity="Comment",mappedBy="image")
     */
    private $comments;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $user
     *
     * @return Image
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Comment[] $comments
     * @return Image
     */
    public function setComments($comments)
    {
        $this->comments[] = $comments;
        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param File $imageFile
     * @return Image
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $image
     * @return Image
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $user User
     * @return Image
     */
    public function addLiker($user)
    {
        $this->likers[] = $user;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * @var $user User
     * @return Image
     */
    public function removeLiker($user)
    {
        $this->likers->removeElement($user);
        return $this;
    }
}

