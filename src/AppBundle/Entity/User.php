<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\JoinColumn;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="user")
     * @ORM\JoinTable(name="user_user", joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="follower_id", referencedColumnName="id")})
     */
    private $followers;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="user",mappedBy="followers")
     */
    private $user_follow;

    /**
     * @var Image[]
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user")
     */
    private $images;

    /**
     * @var Comment[]
     * @ORM\OneToMany(targetEntity="Comment",mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->user_follow = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable= true, unique= false)
     */
    private $avatar;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var \DateTime
     * @ORM\Column(name="update_at",type="datetime",nullable=true)
     */
    private $updateAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->email ?: '';
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param \DateTime $updateAt
     * @return User
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param Image[] $images
     * @return User
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Comment[] $comments
     * @return User
     */
    public function setComments($comments)
    {
        $this->comments[] = $comments;
        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return User[]
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param $follower User
     * @return User
     */
    public function addFollower($follower)
    {
        $this->followers[] = $follower;
        return $this;
    }

    /**
     * @param User $user
     * @return User
     */
    public function removeFollower($user)
    {
        $this->followers->removeElement($user);
        return $this;
    }

    /**
     * @param User[] $user_follow
     * @return User
     */
    public function setUserFollow($user_follow)
    {
        $this->user_follow[] = $user_follow;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getUserFollow()
    {
        return $this->user_follow;
    }

    /**
     *
     */
    public function getUserFollowId()
    {
        $usersId[] = $this->getId();
        $users = $this->getUserFollow();
        foreach ($users as $value) {
            $usersId[] = $value->getId();
        }

        return $usersId;
    }
}
